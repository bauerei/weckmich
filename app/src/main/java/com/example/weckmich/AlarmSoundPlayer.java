package com.example.weckmich;

interface AlarmSoundPlayer {

    void play();

    void stop();

    boolean isPlaying();

    void setAlarmSound(AlarmSound alarmSound);

    void release();

    void setOnCompletionListener(OnCompletionListener listener);

    interface OnCompletionListener {
        void OnCompletion();
    }
}
