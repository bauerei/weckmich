package com.example.weckmich;

import android.net.Uri;
import android.os.Parcelable;

import com.google.auto.value.AutoValue;

/**
 * An AlarmSound keeps condensed metadata about sound files.
 */
@AutoValue
abstract class AlarmSound implements Parcelable {
    private Uri contentUriBase;

    abstract long getId();

    abstract String getArtist();

    abstract String getTitle();

    abstract int getAlbumId();

    /**
     * States if AlarmSound can be used as alarm.
     * <p>
     * Audio files need to be declared explicitly as alarm in Android in order to use them
     * as alarm sound.
     *
     * @return True if AlarmSound is declared as alarm in Android
     */
    abstract boolean isAlarm();

    /**
     * Get Uri of the actual path on a physical drive to the sound file.
     * <p>
     * Example: /storage/1EEA-19FB/Music/Artist/Album/01 - Song.mp3
     *
     * @return File path on physical drive
     * @see AlarmSound#getContentUri() get Android's content:// style Uri
     */
    abstract Uri getPath();

    /**
     * Get base of content:// style Uri.
     * Should be either one of
     * {@link android.provider.MediaStore.Audio.Media#EXTERNAL_CONTENT_URI} or
     * {@link android.provider.MediaStore.Audio.Media#INTERNAL_CONTENT_URI}
     *
     * @return Base of content:// style Uri
     */
    abstract Uri getContentUriBase();

    /**
     * Get Uri of file in Android's content:// style.
     * <p>
     * The content Uri is needed by some Android classes
     * (e.g. {@link android.media.MediaPlayer} for playing the AlarmSound or
     * {@link android.media.RingtoneManager} for setting an AlarmSound as current alarm.
     * <br/>
     * The content Uri is the base content Uri given in {@link AlarmSound#create(long, String, String, boolean, String, Uri)} with {@link AlarmSound#getId()} appended.
     * <br/>
     * Example: content://media/external/audio/media/1
     *
     * @return content:// style Uri of sound
     * @see AlarmSound#getPath() get actual file path
     */
    Uri getContentUri() {
        return contentUriBase
                .buildUpon()
                .appendPath(Long.toString(getId()))
                .build();
    }

    /**
     * Creates a new alarm sound with given metadata.
     *
     * @param isAlarm        States if the alarm sound is registered in Android as an alarm sound (see {@link AlarmSound#isAlarm()})
     * @param path           Local file path on physical drive to sound file (see {@link AlarmSound#getPath()})
     * @param contentUriBase Base of Android's content:// style Uri (see {@link AlarmSound#getContentUri()}).
     *                       Should be either one of
     *                       {@link android.provider.MediaStore.Audio.Media#EXTERNAL_CONTENT_URI} or
     *                       {@link android.provider.MediaStore.Audio.Media#INTERNAL_CONTENT_URI}
     */
    static AlarmSound create(long id, String artist, String title, int albumId, boolean isAlarm, String path, Uri contentUriBase) {
        Uri parsedPath = Uri.parse(path);

        AlarmSound alarmSound = new AutoValue_AlarmSound(id, artist, title, albumId, isAlarm, parsedPath, contentUriBase);
        alarmSound.contentUriBase = contentUriBase;

        return alarmSound;
    }

    /**
     * Creates a new alarm sound without an album id
     */
    static AlarmSound createWithoutAlbumId(long id, String artist, String title, boolean isAlarm, String path, Uri contentUriBase) {
        int noAlbumAssignedId = 0;

        return create(id, artist, title, noAlbumAssignedId, isAlarm, path, contentUriBase);
    }

    /**
     * Get a {@link AlarmSound.Builder} with values based on the object, where this method is called
     *
     * @return Builder to create a new {@link AlarmSound} object
     */
    abstract Builder toBuilder();

    static Builder builder() {
        return new AutoValue_AlarmSound.Builder();
    }

    @AutoValue.Builder
    abstract static class Builder {
        abstract Builder setId(long id);

        abstract Builder setArtist(String artist);

        abstract Builder setTitle(String title);

        abstract Builder setAlbumId(int albumId);

        abstract Builder setAlarm(boolean isAlarm);

        abstract Builder setPath(Uri path);

        abstract Builder setContentUriBase(Uri contentUriBase);

        abstract AlarmSound build();
    }
}
