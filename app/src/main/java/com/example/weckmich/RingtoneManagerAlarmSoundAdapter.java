package com.example.weckmich;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

class RingtoneManagerAlarmSoundAdapter implements AlarmSoundAdapter {
    private static final String TAG = "AlarmSoundAdapter";

    private Context context;

    RingtoneManagerAlarmSoundAdapter(Context context) {
        this.context = context;
    }

    @Override
    public AlarmSound setDefaultAlarmSound(AlarmSound newAlarmSound) {
        Uri alarmSoundUri = getAlarmSoundUri(newAlarmSound);

        Log.d(TAG, String.format("Set URI %s as new alarm sound", alarmSoundUri));
        RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_ALARM, alarmSoundUri);

        if (isCurrentAlarmSound(alarmSoundUri)) {
            return newAlarmSound.toBuilder()
                    .setAlarm(true)
                    .build();
        } else {
            throw new AlarmSoundAdapterException(String.format("Could not set %s as new alarm sound", alarmSoundUri));
        }
    }

    @Override
    public AlarmSound getCurrentAlarmSound() {
        Uri currentAlarmSoundUri = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_ALARM);

        if (currentAlarmSoundUri == null) {
            throw new NoneAlarmSoundException("Default alarm sound is set to none");
        }

        Log.d(TAG, String.format("Convert current alarm sound uri %s to AlarmSound", currentAlarmSoundUri));
        return getAlarmSoundFromUri(currentAlarmSoundUri);
    }

    @Override
    public void removeAlarmSound(AlarmSound alarmSound) {
        if (isExternal(alarmSound)) {
            ContentValues alarmSoundProperty = new ContentValues();
            alarmSoundProperty.put(MediaStore.Audio.AudioColumns.IS_ALARM, false);

            int rowsUpdated = context.getContentResolver().update(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    alarmSoundProperty,
                    String.format("%s=%s", MediaStore.Audio.AudioColumns._ID, alarmSound.getId()),
                    null);

            if (rowsUpdated != 1) {
                throw new AlarmSoundAdapterException(String.format("Could not remove property IS_ALARM from %s (%s - %s). Expected to update 1 row, but actually updated %d rows",
                        alarmSound.getContentUri(), alarmSound.getArtist(), alarmSound.getTitle(), rowsUpdated));
            }
        }
    }

    private boolean isExternal(AlarmSound alarmSound) {
        return alarmSound.getContentUriBase().equals(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
    }

    private Uri getAlarmSoundUri(AlarmSound alarmSound) {
        if (!alarmSound.isAlarm()) {
            registerAsAlarmSound(alarmSound);
        }

        return alarmSound.getContentUri();
    }

    private void registerAsAlarmSound(AlarmSound alarmSound) {
        if (isExternal(alarmSound)) {
            ContentValues alarmSoundProperty = new ContentValues();
            alarmSoundProperty.put(MediaStore.Audio.AudioColumns.IS_ALARM, true);

            context.getContentResolver().update(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    alarmSoundProperty,
                    String.format("%s=%s", MediaStore.Audio.AudioColumns._ID, alarmSound.getId()),
                    null);
        }
    }

    private boolean isCurrentAlarmSound(Uri alarmSoundUri) {
        Uri currentAlarmSoundUri = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_ALARM);
        Log.d(TAG, String.format("New alarm sound URI: %s", currentAlarmSoundUri));

        return currentAlarmSoundUri.getPath().equals(alarmSoundUri.getPath());
    }

    private AlarmSound getAlarmSoundFromUri(Uri alarmSoundUri) {
        try (Cursor cursor = queryCandidate(alarmSoundUri)) {
            return convertToAlarmSound(cursor, getConentUriBase(alarmSoundUri));
        }
    }

    private Cursor queryCandidate(Uri alarmSoundUri) {
        String[] projection = new String[]{
                MediaStore.Audio.AudioColumns._ID,
                MediaStore.Audio.AudioColumns.ARTIST,
                MediaStore.Audio.AudioColumns.TITLE,
                MediaStore.Audio.AlbumColumns.ALBUM_ID,
                MediaStore.Audio.AudioColumns.IS_ALARM,
                MediaStore.Audio.Media.DATA
        };

        Cursor cursor = context.getContentResolver().query(getConentUriBase(alarmSoundUri),
                projection,
                String.format("%s=%s", MediaStore.Audio.AudioColumns._ID, ContentUris.parseId(alarmSoundUri)),
                null,
                null);

        if (cursor.getCount() > 0) {
            cursor.moveToNext();
            return cursor;
        } else {
            throw new AlarmSoundAdapterException("Could not get current alarm sound. Cursor is empty");
        }
    }

    private AlarmSound convertToAlarmSound(Cursor cursor, Uri contentUriBase) {
        int idColumn = cursor.getColumnIndex(MediaStore.Audio.AudioColumns._ID);
        int artistColumn = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST);
        int titleColumn = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE);
        int albumColumn = cursor.getColumnIndex(MediaStore.Audio.AlbumColumns.ALBUM_ID);
        int isAlarmColumn = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.IS_ALARM);
        int pathColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);

        boolean isAlarm = cursor.getInt(isAlarmColumn) != 0 ? true : false;

        return AlarmSound.create(cursor.getLong(idColumn),
                cursor.getString(artistColumn),
                cursor.getString(titleColumn),
                cursor.getInt(albumColumn),
                isAlarm,
                cursor.getString(pathColumn),
                contentUriBase);
    }

    private Uri getConentUriBase(Uri uri) {
        String idPart = uri.getLastPathSegment();
        return Uri.parse(uri.toString().replace(idPart, ""));
    }
}
