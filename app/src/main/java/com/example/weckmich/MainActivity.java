package com.example.weckmich;

import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.io.FileNotFoundException;

import io.gresse.hugo.vumeterlibrary.VuMeterView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    private static final String BUNDLE_CURRENT_ALARM = "currentAlarmSound";
    private static final String BUNDLE_DISCARD_BUTTON_VISIBILITY = "discardButtonVisibility";
    private static final String BUNDLE_APPLY_BUTTON_VISIBILITY = "applyButtonVisibility";

    private final int MAX_DRAW_RETRIES = 5;
    private int drawRetries = 0;

    //UI elements
    private TextView currentTitleView;
    private TextView currentArtistView;
    private ImageButton discardButton;
    private ImageButton applyButton;
    private ImageButton playButton;
    private ImageView coverView;
    private VuMeterView equalizerView;

    //Dependencies
    private RandomAlarmPicker alarmPicker;
    private AlarmSoundAdapter alarmSoundAdapter;
    private AlarmSoundPlayer alarmSoundPlayer;
    private PermissionManager permissionManager;
    private AlarmSoundDatabase alarmSoundDatabase;
    private AlbumCoverFetcher albumCoverFetcher;

    //Fields
    private AlarmSound currentAlarmSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUI();
        initializeDependencies();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (currentAlarmSound != null) {
            setRandomAlarmPickerResult(currentAlarmSound);
        } else {
            showCurrentAlarmSound();
        }

        positionPlayButton();

    }

    private void positionPlayButton() {
        Point displaySize = getDisplaySize();

        playButton.measure(displaySize.x, displaySize.y);
        currentArtistView.measure(displaySize.x, displaySize.y);
        currentTitleView.measure(displaySize.x, displaySize.y);

        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) playButton.getLayoutParams();
        layoutParams.bottomMargin = currentArtistView.getMeasuredHeight() + currentTitleView.getMeasuredHeight() - playButton.getMeasuredHeight() / 2
                + getResources().getDimensionPixelSize(R.dimen.padding_round_button);
        playButton.setLayoutParams(layoutParams);
    }

    private Point getDisplaySize() {
        Display display = getWindowManager().getDefaultDisplay();
        Point displaySize = new Point();
        display.getSize(displaySize);

        return displaySize;
    }

    @Override
    protected void onStop() {
        super.onStop();
        alarmSoundPlayer.release();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(BUNDLE_CURRENT_ALARM, currentAlarmSound);
        outState.putInt(BUNDLE_DISCARD_BUTTON_VISIBILITY, discardButton.getVisibility());
        outState.putInt(BUNDLE_APPLY_BUTTON_VISIBILITY, applyButton.getVisibility());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        currentAlarmSound = savedInstanceState.getParcelable(BUNDLE_CURRENT_ALARM);
        discardButton.setVisibility(savedInstanceState.getInt(BUNDLE_DISCARD_BUTTON_VISIBILITY));
        applyButton.setVisibility(savedInstanceState.getInt(BUNDLE_DISCARD_BUTTON_VISIBILITY));
    }

    private void initializeUI() {
        currentTitleView = findViewById(R.id.label_title);
        currentArtistView = findViewById(R.id.label_artist);
        discardButton = findViewById(R.id.button_discard);
        applyButton = findViewById(R.id.button_apply);
        playButton = findViewById(R.id.button_play);
        coverView = findViewById(R.id.image_Cover);
        equalizerView = findViewById(R.id.equalizer);
    }

    private void hideDiscardAndApply() {
        discardButton.setVisibility(View.INVISIBLE);
        applyButton.setVisibility(View.INVISIBLE);
    }

    private void showDiscardAndApplyButtons() {
        discardButton.setVisibility(View.VISIBLE);
        applyButton.setVisibility(View.VISIBLE);
    }

    private void initializeDependencies() {
        alarmPicker = new MediaStoreRandomAlarmPicker(getContentResolver());
        alarmSoundAdapter = new RingtoneManagerAlarmSoundAdapter(this);

        alarmSoundPlayer = new AlarmSoundMediaPlayer(getApplicationContext());
        alarmSoundPlayer.setOnCompletionListener(() -> {
            onPlayComplete();
        });

        permissionManager = new SimplePermissionManager(this);
        alarmSoundDatabase = new AlarmSoundDatabaseHelper(this);
        albumCoverFetcher = new MediaStoreAlbumCoverFetcher(getContentResolver());
    }

    private void onPlayComplete() {
        Log.d(TAG, "Alarm sound preview completed");
        togglePlayerVisuals();
    }

    private void togglePlayerVisuals() {
        if (alarmSoundPlayer.isPlaying()) {
            playButton.setImageResource(R.drawable.ic_pause);
            startEqualizerAnimation();
        } else {
            playButton.setImageResource(R.drawable.ic_play);
            stopEqualizerAnimation();
        }
    }

    private void startEqualizerAnimation() {
        if (equalizerView.isShown()) {
            equalizerView.resume(true);
        }
    }

    private void stopEqualizerAnimation() {
        if (equalizerView.isShown()) {
            equalizerView.stop(true);
        }
    }

    public void onClickDiscard(View view) {
        showCurrentAlarmSound();
    }

    private void showCurrentAlarmSound() {
        if (permissionManager.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            try {
                currentAlarmSound = alarmSoundAdapter.getCurrentAlarmSound();

                setMetaDataView(currentAlarmSound);
                setPlayer(currentAlarmSound);
                setAlbumCover(currentAlarmSound);
                hideDiscardAndApply();
            } catch (NoneAlarmSoundException e) {
                Log.e(TAG, "Current alarm sound is set to none", e);
                setMetaDataToNoneAlarm();
            } catch (AlarmSoundAdapterException e) {
                Log.e(TAG, "Error getting current alarm sound", e);
            }
        } else {
            permissionManager.queryPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    public void onClickDraw(View view) {
        if (permissionManager.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            pickRandomAlarm();
        } else {
            permissionManager.queryPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    public void onClickApply(View view) {
        if (permissionManager.hasSettingsWritePermission()) {
            try {
                AlarmSound newAlarmSound = alarmSoundAdapter.setDefaultAlarmSound(currentAlarmSound);
                Toast.makeText(getApplicationContext(), R.string.toast_setAlarmSuccess,
                        Toast.LENGTH_SHORT)
                        .show();
                showCurrentAlarmSound();

                removeLastAlarmSound();
                alarmSoundDatabase.saveAlarmSound(newAlarmSound);
            } catch (AlarmSoundAdapterException e) {
                Log.e(TAG,"Could not set alarm sound", e);
                Toast.makeText(getApplicationContext(), R.string.toast_setAlarmFailure,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        } else {
            permissionManager.querySettingsWritePermission();
        }
    }

    public void onClickPlay(View view) {
        if (alarmSoundPlayer.isPlaying()) {
            Log.d(TAG, "Stop preview");
            alarmSoundPlayer.stop();
        } else {
            try {
                Log.d(TAG, "Play preview");
                alarmSoundPlayer.play();
            } catch (AlarmSoundPlayerException e) {
                Log.e(TAG, "Error playing preview", e);
            }
        }
        togglePlayerVisuals();
    }

    private void removeLastAlarmSound() {
        Log.d(TAG, String.format("Get last alarm sound from db"));
        AlarmSound lastAlarmSound = alarmSoundDatabase.getLatestAlarmSound();
        if (lastAlarmSound != null) {
            Log.d(TAG, String.format("Remove last alarm sound %s (%s - %s)",
                    lastAlarmSound.getContentUri(), lastAlarmSound.getArtist(), lastAlarmSound.getTitle()));
            try {
                alarmSoundAdapter.removeAlarmSound(lastAlarmSound);
            } catch (AlarmSoundAdapterException e) {
                Log.e(TAG, "Could not remove last alarm sound", e);
            }
        }
    }

    private void pickRandomAlarm() {
        try {
            AlarmSound drawResult = alarmPicker.drawRandomAlarmSound();
            Log.d(TAG, String.format("Selected alarm sound: %s - %s at %s", currentAlarmSound.getArtist(), currentAlarmSound.getTitle(), currentAlarmSound.getPath()));

            if (drawResult.equals(currentAlarmSound)) {
                retryPickRandomAlarm();
            } else {
                drawRetries = 0;
                currentAlarmSound = drawResult;
                setRandomAlarmPickerResult(currentAlarmSound);
                showDiscardAndApplyButtons();
            }

        } catch (RandomAlarmPickerException e) {
            Log.e(TAG, "Could not get random alarm sound", e);
        }
    }

    private void retryPickRandomAlarm() {
        if (drawRetries < MAX_DRAW_RETRIES) {
            drawRetries++;
            pickRandomAlarm();
        } else {
            drawRetries = 0;
            Toast.makeText(getApplicationContext(), R.string.toast_tooManyRetries, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void setRandomAlarmPickerResult(AlarmSound alarm) {
        setMetaDataView(alarm);
        setAlbumCover(alarm);
        setPlayer(alarm);
    }

    private void setAlbumCover(AlarmSound alarm) {
        try {
            Bitmap albumCover = albumCoverFetcher.getAlbumCover(alarm);
            coverView.setImageBitmap(albumCover);
            int paddingCover = getResources().getDimensionPixelSize(R.dimen.padding_cover_view_cover);
            coverView.setPadding(paddingCover, paddingCover, paddingCover, paddingCover);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Could not get album cover for", e);
            setUnavailableAlbumCover();
        }
    }

    private void setUnavailableAlbumCover() {
        int paddingCover = getResources().getDimensionPixelSize(R.dimen.padding_cover_view_noCover);
        coverView.setPadding(paddingCover, paddingCover, paddingCover, paddingCover);
        coverView.setImageDrawable(getDrawable(R.drawable.ic_no_image));
    }

    private void setPlayer(AlarmSound alarm) {
        Log.d(TAG, "Load alarm sound for preview player");
        try {
            alarmSoundPlayer.setAlarmSound(alarm);
            togglePlayerVisuals();
        } catch (AlarmSoundPlayerException e) {
            Log.e(TAG, "Could not set alarm sound for preview player", e);
        }
    }

    private void setMetaDataView(AlarmSound alarmSound) {
        currentTitleView.setText(alarmSound.getTitle());
        currentArtistView.setText(alarmSound.getArtist());
    }

    private void setMetaDataToNoneAlarm() {
        currentTitleView.setText(getString(R.string.lb_currentAlarmSound_none));
    }
}