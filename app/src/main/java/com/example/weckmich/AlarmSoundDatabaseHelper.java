package com.example.weckmich;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

class AlarmSoundDatabaseHelper extends SQLiteOpenHelper implements AlarmSoundDatabase {
    private static final String TAG = "DatabaseHelper";

    private static final String DB_NAME = "WeckMich";
    private static final int DB_VERSION = 1;

    private static final String TABLE_ALARMS = "Alarms";

    private static final String COLUMN_CONTENT_ID = "CONTENT_ID";
    private static final String COLUMN_ARTIST = "ARTIST";
    private static final String COLUMN_TITLE = "TITLE";
    private static final String COLUMN_IS_ALARM = "IS_ALARM";
    private static final String COLUMN_PATH = "PATH";
    private static final String COLUMN_CONTENT_URI_BASE = "CONTENT_URI_BASE";

    AlarmSoundDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateDatabase(db, oldVersion, newVersion);
    }

    @Override
    public void saveAlarmSound(AlarmSound alarmSound) {
        try (SQLiteDatabase db = getWritableDatabase()) {
            ContentValues newEntry = new ContentValues();
            newEntry.put(COLUMN_CONTENT_ID, alarmSound.getId());
            newEntry.put(COLUMN_ARTIST, alarmSound.getArtist());
            newEntry.put(COLUMN_TITLE, alarmSound.getTitle());
            newEntry.put(COLUMN_IS_ALARM, alarmSound.isAlarm() ? 1 : 0);
            newEntry.put(COLUMN_PATH, alarmSound.getPath().toString());
            newEntry.put(COLUMN_CONTENT_URI_BASE, alarmSound.getContentUriBase().toString());

            long newEntryId = db.insert(TABLE_ALARMS, null, newEntry);
            if (newEntryId == -1) {
                Log.e(TAG, "Received error code on saving alarm sound to DB");
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "Could not open DB", e);
        }
    }

    @Override
    public AlarmSound getLatestAlarmSound() {
        try (SQLiteDatabase db = getReadableDatabase()) {
            try (Cursor cursor = db.query(TABLE_ALARMS,
                    null,
                    null,
                    null,
                    null,
                    null,
                    "_id DESC",
                    Integer.toString(1))) {

                if (cursor.moveToFirst()) {
                    long id = cursor.getInt(1);
                    String artist = cursor.getString(2);
                    String title = cursor.getString(3);
                    boolean isAlarm = cursor.getInt(4) == 1 ? true : false;
                    String path = cursor.getString(5);
                    Uri contentUriBase = Uri.parse(cursor.getString(6));

                    return AlarmSound.createWithoutAlbumId(id, artist, title, isAlarm, path, contentUriBase);
                }
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "Could not open DB", e);
        }

        return null;
    }

    private void updateDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            initDatabase(db);
        }
    }

    private void initDatabase(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE %s (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "%s INTEGER, " +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s INTEGER," +
                        "%s TEXT," +
                        "%s TEXT);",
                TABLE_ALARMS, COLUMN_CONTENT_ID, COLUMN_ARTIST, COLUMN_TITLE, COLUMN_IS_ALARM, COLUMN_PATH, COLUMN_CONTENT_URI_BASE));
    }
}
