package com.example.weckmich;

class AlarmSoundPlayerException extends RuntimeException {
    AlarmSoundPlayerException(String message) {
        super(message);
    }

    AlarmSoundPlayerException(String message, Throwable cause) {
        super(message, cause);
    }
}
