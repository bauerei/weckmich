package com.example.weckmich;

interface PermissionManager {
    boolean hasPermission(String permission);

    void queryPermission(String permission, int permissionRequestCode);

    boolean hasSettingsWritePermission();

    void querySettingsWritePermission();
}
