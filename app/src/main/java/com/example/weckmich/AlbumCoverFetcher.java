package com.example.weckmich;

import android.graphics.Bitmap;

import java.io.FileNotFoundException;

interface AlbumCoverFetcher {
    Bitmap getAlbumCover(AlarmSound alarmSound) throws FileNotFoundException;
}
